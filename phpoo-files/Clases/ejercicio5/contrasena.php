<?php
    class Contrasena{             //Declaramos una nueva clase llamada Contrasena
        public $contrasena;       //Agregamos un atributo publico donde se guardará la contraseña
        public function __construct(){   //Se crea el metodo constructor
            $abc='ABCDEFGHIJKLMNÑOPQRSTUVWXYZ'; //Declaramos una cadena que nos ayudará a generar la contraseña
            $this->contrasena=substr(str_shuffle($abc),0,4); //Creamos la contraseña haciendo un reordenamiento con str_shuffle
        }                                                    // y tomamos los primeros cuatro acracteres con substr
        public function __destruct(){     //Creamos el metodo destructor
            echo 'Tu contraseña es: '.$this->contrasena.'<br>';  //Imprimimos en pantalla la contraseña al destruir el objeto
        }
    }

    if (!empty($_POST)){  //Verificamos que hayamos recibido algo con el metodo POST
        $nuevaContraseña = new Contrasena(); //Creamos un objeto instanciando la clase Contrasena
    }



?>