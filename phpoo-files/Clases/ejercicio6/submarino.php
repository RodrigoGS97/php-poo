<?php
class submarino extends transporte{  //Declaramos una nueva clase que hereda de transporte
        private $profundidad;     //Añadimos un atributo con respecto a este tranporte

        //sobreescritura de constructor
        public function __construct($nom,$vel,$com,$pro){ //El constructor es igual al de las otras clases
            parent::__construct($nom,$vel,$com);          //Recibe las variables necesarias para la clase padre para inicializar atributos
            $this->profundidad=$pro;                      //Y despues inicializamos el atributo de esta clase por separado
        }

        // sobreescritura de metodo
        public function resumenSubmarino(){      //Declaramos este metodo para crear un mensaje
            $mensaje=parent::crear_ficha();      //La parte principal de este mensaje se crea con la función de la clase padre
            $mensaje.='<tr>                    
                        <td>Profundidad:</td>
                        <td>'. $this->profundidad.'</td>				
                    </tr>';                      //Y añadimos el resto del mensaje por separado
            return $mensaje;                     //Finalmente regresamos el mensaje
        }
    }
?>    