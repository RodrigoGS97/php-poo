<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $resultadoVeri; //declaramos el atributo en privado

	//declaracion del método verificación
	public function verificacion($anoVeri){
		if($anoVeri < 1990 ){ //la función resivirá el valor de los años y lo pasaremos por una serie de if 
			$this->resultadoVeri='No'; //El atributo privado obtendrá su valor dependiendo del valor
		}elseif ($anoVeri >= '1990-01' && $anoVeri <= '2010-12' ) {
			$this->resultadoVeri='Revisión';
		}else{
			$this->resultadoVeri='Si';
		}
	}
	public function get_resultadoVeri(){ //declaramos otro metodo dentro de la clase, para obtener el valor del atributo privado
		return $this->resultadoVeri;
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->verificacion($_POST['AnoFabricacion']); //llamamos a la función para que el atributo privado tenga valor.
	
}




